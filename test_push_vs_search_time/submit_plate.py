from random import choice
from string import ascii_uppercase, digits
from time import sleep
from datetime import datetime
import json
from pprint import pprint
from urllib.parse import urlencode
import os

import requests
import datadog


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def get_config():
    with open(os.path.join(BASE_DIR, 'config.json')) as buff:
        return json.loads(buff.read())


config = get_config()
statsd = datadog.DogStatsd(
    host=config['datadog_statsd_host'], port=config['datadog_statsd_port'],
    namespace=config['datadog_app_name'], constant_tags=['name:value']
)


def generate_plate(length=8):
    return str.join('', [choice(ascii_uppercase + digits) for _ in range(length)])  # noqa


def submit_plate(plate):
    pprint(plate)

    with open(os.path.join(BASE_DIR, 'plate-data.json')) as buff:
        data = json.loads(buff.read())
        data['company_id'] = config['company_id']
        data['agent_uid'] = config['agent_id']
        data['camera_id'] = config['camera_id']
        data['best_plate']['plate'] = plate
        data['best_plate_number'] = plate
        data['epoch_start'] = int(datetime.now().timestamp() * 1000)
        data['epoch_end'] = int(datetime.now().timestamp() * 1000) + 3000

        resp = requests.post(config['submit_url'], data=json.dumps(data), verify=False)
        pprint(resp)

        statsd.increment(
            'plates-consistency', 1, tags=[
                'company_id:{}'.format(config['company_id']),
                'type:plates-sent'
            ]
        )


def query_search_api(start_time):
    request_data = {
        'start': start_time.isoformat(),
        'end': datetime.now().isoformat(),
        'company_id': config['company_id'],
    }
    request_data = urlencode(request_data)
    request_url = config['search_url'] + f'?{request_data}'

    pprint(request_url)

    resp = requests.get(request_url, verify=False)

    if resp.status_code == 200:
        pprint('Plates: {}'.format(len(resp.json())))

        if len(resp.json()):
            statsd.increment(
                'plates-consistency', 1,
                tags=[
                    'company_id:{}'.format(config['company_id']),
                    'type:plates-received'
                ]
            )
    else:
        pprint(resp.text)


if __name__ == '__main__':
    test_time = datetime.now()

    plate = generate_plate()
    submit_plate(plate)

    sleep(config['sleep_after_submit'])
    query_search_api(test_time)
