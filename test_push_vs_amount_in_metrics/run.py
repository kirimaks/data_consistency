from pprint import pprint
from datetime import datetime
from urllib.parse import urlencode, urlsplit, urlunsplit
import os.path
import json
import sys
from random import choice
from string import ascii_uppercase, digits
from time import sleep
import urllib3

import requests
import datadog


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def get_config():
    with open(os.path.join(BASE_DIR, "config.json")) as buff:
        return json.loads(buff.read())


CONFIG = get_config()
LOGIN_URL = '{}/account/login'.format(CONFIG['base_url'])
SUBMIT_PLATE_URL = '{}/push'.format(CONFIG['base_url'])
STATSD = datadog.DogStatsd(
    host=CONFIG['datadog_statsd_host'], port=CONFIG['datadog_statsd_port'],
    namespace=CONFIG['datadog_app_name'], constant_tags=['name:value']
)


def get_login_request_data(csrf_token):
    return {
        # 'username': 'kirimaks@yahoo.com',
        # 'password': '1234',
        'username': CONFIG['username'],
        'password': CONFIG['password'],
        'csrfmiddlewaretoken': csrf_token,
    }


def get_metrics_daily_url():
    start_time = datetime.utcnow().replace(
        hour=0, minute=0, second=0, microsecond=0
    ).timestamp() * 1000
    start_time = int(start_time)

    end_time = datetime.utcnow().replace(
        hour=23, minute=59, second=59, microsecond=999999
    ).timestamp() * 1000
    end_time = int(end_time)

    request_data = {
        'start': start_time,
        'end': end_time,
        'granularity': 3600,
    }

    metrics_url = list(urlsplit(CONFIG['base_url']))
    metrics_url[2] = '/api/statistics/all_cameras'
    metrics_url[3] = urlencode(request_data)
    metrics_url = urlunsplit(metrics_url)

    return metrics_url


def login(session):
    resp = ss.get(LOGIN_URL, verify=False)
    assert resp.status_code == 200

    csrf_token = resp.cookies['csrftoken']
    request_data = get_login_request_data(csrf_token)
    resp = ss.post(LOGIN_URL, data=request_data, verify=False)

    assert resp.status_code == 200
    assert 'api_key' in resp.json()

    return True


def get_plates_today_for_camera(session, camera_id, agent_id):
    metrics_url = get_metrics_daily_url()
    resp = session.get(metrics_url, verify=False)

    assert resp.status_code == 200

    resp = resp.json()

    try:
        cam_nodes = filter(lambda i: i['camera_id'] == str(camera_id) and i['agent_uid'] == agent_id, resp.get('cameras'))
        cam_nodes = list(cam_nodes)

        cam_node = cam_nodes[0]

        camera_pk = str(cam_node['camera_pk'])
        camera_name = cam_node['camera_name']

        pprint(f'Camera pk: {camera_pk}')
        pprint(f'Camera name: {camera_name}')

    except Exception as e:
        pprint(f'Error: Camera {camera_id} not found - {e}')
        sys.exit(1)

    try:
        cam_results = resp['results'][camera_name][camera_pk]
        total_plates = [int(d.get('plate_group_total', 0)) for d in cam_results.values()]
        total_plates = sum(total_plates)

        return total_plates

    except Exception as e:
        pprint(f'Error: Cannot get data for camera {camera_id} - {e}')
        sys.exit(1)

    return 0


def generate_plate(length=8):
    return str.join('', [choice(ascii_uppercase + digits) for _ in range(length)])  # noqa


def submit_plate(plate):
    pprint(plate)

    with open(os.path.join(BASE_DIR, 'plate-data.json')) as buff:
        data = json.loads(buff.read())
        data['company_id'] = CONFIG['company_id']
        data['agent_uid'] = CONFIG['agent_id']
        data['camera_id'] = CONFIG['camera_id']
        data['best_plate']['plate'] = plate
        data['best_plate_number'] = plate
        data['epoch_start'] = int(datetime.now().timestamp() * 1000)
        data['epoch_end'] = int(datetime.now().timestamp() * 1000) + 3000

        resp = requests.post(SUBMIT_PLATE_URL, data=json.dumps(data), verify=False)
        assert resp.status_code == 200


def submit_plates(num_of_plates_to_submit):
    for _ in range(num_of_plates_to_submit):
        plate = generate_plate()
        submit_plate(plate)


if __name__ == '__main__':
    with requests.Session() as ss:
        if login(ss):
            camera_id = CONFIG['camera_id']
            agent_id = CONFIG['agent_id']
            plates_to_submit = CONFIG['num_of_plates_to_submit']

            plates_before_submit = get_plates_today_for_camera(ss, camera_id, agent_id)
            pprint('(before): Plates today for camera {}: {}'.format(
                camera_id, plates_before_submit)
            )

            submit_plates(plates_to_submit)
            sleep(40)

            plates_after_submit = get_plates_today_for_camera(ss, camera_id, agent_id)
            pprint('(after): Plates today for camera {}: {}'.format(
                camera_id, plates_after_submit)
            )

            plates_processed = plates_after_submit - plates_before_submit

            pprint('Plates submitted: {}'.format(plates_to_submit))
            pprint('Plates processed: {}'.format(plates_processed))

            STATSD.histogram(
                CONFIG['datadog_stats_name'], plates_to_submit, tags=['type:submitted']
            )
            STATSD.histogram(
                CONFIG['datadog_stats_name'], plates_processed, tags=['type:processed']
            )
